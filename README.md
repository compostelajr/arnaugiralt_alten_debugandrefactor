# Debug + refactor código

El fetching y procesado de datos se ha refactorizado utilizando promesas (async/await también valdría, pero por cosas de compatibilidad aún funcionan mejor las promesas).
De este modo, el hilo principal puede seguir trabajando sin necesidad de bloquearse

---

### Modificación de los datos originales

Modificar los datos originales en `calcPercent()` impedía obtener el valor de los habitantes por género por comarca directamente, además de ser una mala práctica la modificación de los datos originales.

Original:
```javascript
function calcPercent(data, total) {
    data.OBS_VALUE = parseFloat(Number(data.OBS_VALUE) / Number(total) *100).toFixed(2);
    return data.OBS_VALUE;
}
```

Refactorizado:
```javascript
function calcPercent(data, total) {
	return parseFloat(Number(data) / Number(total) *100).toFixed(2);
}
```


### Cálculo del número de habitantes por género por comarca

Aquí hay dos opciones, o bien calcularlo por regla de tres con el porcentaje del género en cuestión y el total de habitantes de la comarca, o simplemente coger el valor des de el dataset, en `comarca['cross:DataSet']['cross:Section']['cross:Obs'][0].OBS_VALUE`. Esto es posible ahora ya que se ha modificado `calcPercent()`, y `OBS_VALUE` vuelve a ser el original. Igualmente, he escrito la versión de cálculo, que está comentada, por si queréis verla.



### Suma del número total de habitantes por comarca

Igual que en el punto anterior, este se puede solucionar obteniendo el valor directamente des de el dataset, en `comarca['cross:DataSet']['cross:Section']['cross:Obs'][2].OBS_VALUE` . Como supuse que queríais que solucionase la suma, esa es la solución que he elegido.
Las variables `comarcaHabitantesHombres` y `comarcaHabitantesMujeres` almacenan el valor del número de habitantes por género en la comarca, y es esta variable la que se suma a los totales, por género y general.